function Person(name){
	this.name = name
}
Person.prototype.sayHello = function sayHello(){ return 'Hi, I am ' + this.name }

var alice = new Person('Alice');
var bob = new Person('Bob');

function Employee(name, salary){
	Person.apply(this,[name])
	this.salary = salary;
}
Employee.prototype = Object.create(Person.prototype)
Employee.prototype.work = function(){ return 'I want my '+this.salary }

var tom = new Employee('Tom',3500);

tom instanceof Employee
tom instanceof Person

// ES6 EcmaScript2015

class Person{
	constructor(name){ this.name = name }

	sayHello(){ return 'Hi, I am '+this.name }
}

class Employee extends Person{

	constructor(name,salary){
		super(name);
		this.salary = salary
    }
	work(){ return 'I want my ' +this.salary }
}

tom = new Employee('Tom',4000);



function Scope(){
	this.$id = Scope.id++
}
Scope.id = 1;

Scope.prototype.$new = function(){
	var childScope = Object.create(this);
	Scope.apply(childScope)
	childScope.$parent = this;
	return childScope;
}

var root = new Scope()
var s1 = root.$new()
var s1_1 = s1.$new()
root.placki = 123;
console.log(s1.placki)