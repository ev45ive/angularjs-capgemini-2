git clone https://bitbucket.org/ev45ive/angularjs-capgemini-2
cd angularjs-capgemini-2

npm install
npm start

# Nowy projekt

npm init

# Instalacja pakietów do node_modules/

npm i --save angular
npm i --save-dev http-server

# C:\Users\<user>\AppData\Roaming\npm\

npm i -g http-server
%PATH%
http-server

# Scripts

npm start
npm run <custom_script_name>

# Bootstrap CSS

npm install bootstrap --save

# Webpack

npm i -g webpack webpack-cli

npm i -D webpack webpack-cli html-webpack-plugin webpack-dev-server 


# Transpilacja ES201.. -> ES5 ( IE 11 )
npm i -D @babel/core @babel/plugin-proposal-class-properties @babel/preset-env babel-loader
babel-plugin-syntax-dynamic-import 

# Import plikow html jako string w JS
npm i -D html-loader 

# Import i kompilacja CSS/SCSS
npm i -D node-sass sass-loader style-loader css-loader


https://jsfiddle.net/ hd o5 ps fu/ 1/


# Formularze

npm install --save angular-messages

# ROuting 
npm install --save @uirouter/angularjs
var myApp = angular.module('helloworld', ['ui.router']);


# Users

touch src/users/users.module.js
touch src/users/users.service.js

touch src/users/users.component.js

touch src/users/users-list.component.js
touch src/users/user-details.component.js
touch src/users/user-todos.component.js

# REST
npm i --save json-server

npm i -g json-server

json-server http://jsonplaceholder.typicode.com/db


# Testing


# Karma
npm i --global karma-cli
npm i --save karma
karma init
- jasmine

# Webpack + Sourcemaps
npm i -D karma-webpack karma-sourcemap-loader

# Jasmine
npm i --save-dev karma  karma-jasmine karma-chrome-launcher jasmine-core 

# Reporters
npm i -D karma-jasmine-html-reporter karma-junit-reporter

# Mocks
npm i -D angular-mocks