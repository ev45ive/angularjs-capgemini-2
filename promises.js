function echo(msg){
	return new Promise(function(resolve){
        setTimeout(()=>{
            resolve(msg)
        },2000)
    })
}

p = echo('Ala').then(function(resp){ return resp + ' ma ' })

p1 = p.then(function(resp){ return (resp + ' kota ') })
p2 = p.then(function(resp){ return echo(resp + ' placki ') })

p1.then(function(resp){ console.log(resp) })
p2.then(function(resp){ console.log(resp) })



function echo(msg,fail){
	return new Promise(function(resolve, reject){
        setTimeout(()=>{
            fail? reject('upss... '+fail) : resolve(msg)
        },2000)
    })
}

p = echo('Ala',true).then(function(resp){ return resp + ' ma ' })

p1 = p.then(function(resp){ return (resp + ' kota ') })
p2 = p.then(function(resp){ return echo(resp + ' placki ') })
p3 = p.then(function(resp){ return echo(resp + ' milion dolarów ',true) })
      .catch(function(err){ return 'Nie ma placków' })

p1.then(function(resp){ console.log(resp) })
p2.then(function(resp){ console.log(resp) })
p3.then(function(resp){ console.log(resp) }, function(err){ console.log('errror',err) })



function echo(msg,fail){
	return new Promise(function(resolve, reject){
        setTimeout(()=>{
            fail? reject('upss... '+fail) : resolve(msg)
        },2000)
    })
}

p = echo('Ala',true).then(function(resp){ return resp + ' ma ' })

p1 = p.then(function(resp){ return (resp + ' kota ') })
p2 = p.then(function(resp){ return echo(resp + ' placki ') })
p3 = p.then(function(resp){ return echo(resp + ' milion dolarów ',true) })
      .catch(function(err){ return Promise.reject('Nie ma placków') })

p1.then(function(resp){ console.log(resp) })
p2.then(function(resp){ console.log(resp) })
p3.then(function(resp){ console.log(resp) }, function(err){ console.log('errror',err) })


