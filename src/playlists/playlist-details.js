const tpl = require("./playlist-details.tpl.html");

angular.module("playlists").directive("playlistDetails", function() {
  return {
    // templateUrl: "src/playlists/playlist-details.tpl.html",
    template: tpl,
    scope: {
      playlist: "<",
      onEdit: "&"
    },
    controller: function($scope) {}
  };
});
