angular.module("playlists").component("playlistsList", {
  templateUrl: "src/playlists/playlists-list.tpl.html",
  bindings: {
    playlists: "<",
    selected: "<",
    onSelect: "&"
  }
});
