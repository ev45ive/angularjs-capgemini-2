var playlists = angular.module("playlists", []);

require('./playlist-details')
require('./playlist-form')

/* --- */
require('./playlists-list')

playlists.service("PlaylistsService", function() {
  console.log("PlaylistsService starting...");

  this.getPlaylists = function() {
    return this.playlists;
  };

  this.save = function(updated) {
    if (!updated.id) {
      updated.id = new Date().getUTCMilliseconds();
      this.playlists.push(updated);
    } else {
      var index = this.playlists.findIndex(p => p.id == updated.id);

      if (index !== -1) {
        this.playlists.splice(index, 1, updated);
      }
    }
  };

  this.playlists = [
    {
      id: 123,
      name: "Angular Greatest Hits",
      favorite: false,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "Angular TOP20",
      favorite: true,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "Best of Angular",
      favorite: true,
      color: "#00ffff"
    }
  ];
});

playlists.controller("PlaylistsCtrl", function($scope, PlaylistsService) {
  // var this = ($scope.vm = this);
  // $scope.vm = this;

  this.playlists = PlaylistsService.getPlaylists();

  this.save = function(updated) {
    PlaylistsService.save(updated);

    this.selected = updated;
    this.mode = "show";
  };

  this.mode = "show";

  this.edit = function() {
    this.draft = Object.assign({}, this.selected);
    this.mode = "edit";
  };

  this.cancel = function() {
    this.selected = null;
    this.mode = "show";
  };

  this.newDraft = function() {
    this.selected = {};
    this.mode = "edit";
  };

  this.selected = null;

  this.select = function(p) {
    this.selected = this.selected === p ? null : p;
  };
});
