// var module = window.module;
var module = angular.mock.module;

require("./playlists.module");

describe("Testing - something", () => {
  var $compile, $rootScope;

  beforeEach(module("playlists"));

  beforeEach(() => {
    inject(function(_$compile_, _$rootScope_) {
      $compile = _$compile_;
      $rootScope = _$rootScope_;
    });
  });

  it("should render playlist", () => {
    var $scope = $rootScope.$new();
    $scope.playlist = {
      name: "TestPlaylist",
      favorite: true,
      color: "#ff00ff"
    };
    $scope.onEdit = jasmine.createSpy("onEdit");

    var element = $compile(
      `<playlist-details playlist="playlist" on-edit="onEdit()"></playlist-details>`
      // {
      //   $scope: $scope
      // }
    )($scope);

    $scope.$digest();

    expect(element[0].querySelector(".playlist-name").textContent).toMatch(
      "TestPlaylist"
    );

    var button = element[0].querySelector("input[type=button]");
    button.click();

    expect($scope.onEdit).toHaveBeenCalled();
  });
});

// https://docs.angularjs.org/guide/unit-testing#testing-a-controller
// https://docs.angularjs.org/guide/component#unit-testing-component-controllers
// https://docs.angularjs.org/api/ngMock/service/$componentController

// https://docs.angularjs.org/api/ngMock/service/$timeout

// wykonaj wyszsykie timery <= 200
// flush(200)
// wykonaj wyszsykie timery
// flush()

// https://docs.angularjs.org/api/ngMock/service/$httpBackend