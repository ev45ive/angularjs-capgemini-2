angular
  .module("playlists")
  .component("playlistForm", {
    templateUrl: "src/playlists/playlist-form.tpl.html",
    bindings: {
      playlist: "<",
      onCancel: "&",
      onSave: "&"
    },
    controller: class PlaylistCtrl {
      constructor() {}

      $onChanges(changesObj) {
        // console.log("$onChanges", changesObj);
        this.draft = Object.assign({}, this.playlist);
      }

      save() {
        if (this.playlistForm.$invalid) {
          return;
        }
        
        this.onSave({
          $event: this.draft
        });
      }

      // =====

      $onInit() {
        console.log("$onInit");
      }

      $doCheck() {
        console.log("$doCheck");
      }

      $onDestroy() {
        console.log("$onDestroy");
      }

      $postLink() {
        console.log("$postLink");

        // this.playlistForm.name.$parsers.push(function(value) {
        // this.playlistForm.name.$formatters.push(function(value) {
        //   return ("" + value).toUpperCase();
        // });

        // this.playlistForm.name.$validators["censor"] = function(
        //   modelValue,
        //   viewValue
        // ) {
        //   // console.log(modelValue, viewValue);
        //   return !("" + viewValue).includes("batman");
        // };
      }
    }
  })

  .directive("censor", function($q) {
    return {
      restrict: "A",
      // requiredDirectives - ngModel is required, form is optionally
      require: ["ngModel", "^?form"],
      link: function(scope, $element, $attrs, requiredDirectives, $ctrl) {
        var [ngModel, form] = requiredDirectives;
        var { name, censor } = $attrs;

        // ngModel.$validators["censor"] = function(modelValue, viewValue) {
        //   return !("" + modelValue).includes("batman");
        // };

        ngModel.$asyncValidators["censor"] = function(modelValue, viewValue) {
          // return $http.get(...).then(resp => ok? true : $q.reject('not ok'))

          return $q(function(resolve, reject) {
            setTimeout(function() {
              if (("" + modelValue).includes("batman")) {
                reject("No Batman!");
              } else {
                resolve(true);
              }
            }, 2000);
          });
        };
      }
    };
  });

/* 
    <div tabs>
      <div tab-name="A"></div>
      <div tab-name="B"></div>
      <div tab-name="B"></div>
    </div>

    
    <form tabs>
      <div tab-name="step1"></div>
      <div tab-name="step1"></div>
      <div tab-name="step1"></div>
    </form>
  */

// class Placki{}
// console.log(Placki)

// angular.module("playlists").directive("playlistForm", function() {
//   return {
//     scope: {
//       playlist: "<",
//       onCancel: "&",
//       onSave: "&"
//     },
//     bindToController: true,
//     templateUrl: "src/playlists/playlist-form.tpl.html",

//     controller: function($scope) {
//       console.log("constructor", this.playlist);

//       this.$onInit = function() {
//         console.log("$onInit", this.playlist);
//         this.draft = Object.assign({}, this.playlist);
//       };
//     },
//     controllerAs: "vm"
//   };
// });
