var users = angular.module("users", []);

require("./user-details.component");
require("./user-todos.component");
require("./users-list.component");
require("./users.component");
require("./users.service");

users.config(function($stateProvider) {
  $stateProvider
    .state({
      name: "users",
      url: "/users",
      redirectTo: "users.list",
      component: "users"
    })
    .state({
      name: "users.list",
      url: "/list",
      resolve: {
        users: function(Users) {
          return Users.fetchUsers();
        }
      },
      views: {
        $default: {
          component: "usersList"
        },
        details:{
          template:'Please select user'
        }
      }
    })
    .state({
      name: "users.list.details",
      url: "/:id/details",
      resolve: {
        user: function(Users, $transition$) {
          return Users.fetchUser($transition$.params().id);
        }
      },
      views: {
        // "$default": {
        //   component: "usersList"
        // },
        "details@users": {
          component: "userDetails"
        }
      }
    });
});
