angular
  .module("users") //
  .component("userDetails", {
    bindings: {
      user: "<"
    },
    template: `<div>
      <dl>
        <dd>Id:</dd>
        <dt>{{$ctrl.user.id}}</dt>

        <dd>Name:</dd>
        <dt>{{$ctrl.user.name}}</dt>
      </dl>

      <button ng-click="$ctrl.cancel()">
        Cancel
      </button>
    </div>`,

    controller: function($state) {
      
      this.cancel = function() {
        // $state.transitionTo('users',{},{})
        $state.go("users");
      };
    }
  });
