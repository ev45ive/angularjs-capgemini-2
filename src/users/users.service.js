angular.module("users").service(
  "Users",
  class Users {
    /**
     * @param {angular.IHttpService} $http
     */
    constructor($http) {
      this.$http = $http;
      this.url = "http://localhost:3000/users/";
    }

    fetchUsers() {
      return this.$http
        .get(this.url, {}) //
        .then(function(resp) {
          return resp.data;
        });
    }

    fetchUser(id) {
      return this.$http
        .get(this.url + id, {}) //
        .then(function(resp) {
          return resp.data;
        });
    }
  }
);

// angular.module("users").service("Users", function($http) {
//   this.url = "http://localhost:3000/users/";

//   this.fetchUsers = function() {
//     return $http.get(this.url);
//   };
// });
