angular
  .module("users") //
  .component("users", {
    template: `<div class="row">
      <div class="col">
        <h1>Users</h1>
        <ui-view></ui-view>
      </div>
      <div class="col">
        <h1></h1>
        <ui-view name="details"></ui-view>
      </div>
    </div>`
  });
