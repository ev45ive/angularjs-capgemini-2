angular
  .module("users") //
  .component("usersList", {
    bindings: { users: "<" },
    template: `<div class="list-group">
      <div class="list-group-item" 
            ng-repeat="user in $ctrl.users"
            ui-sref="users.list.details({ id: user.id })">
        {{user.name}}
      </div>
    </div>`
  });
