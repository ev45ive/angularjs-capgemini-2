import { Subject, BehaviorSubject } from "rxjs";
import { map } from "rxjs/operators";
// import "rxjs/add/operator/map";

angular
  .module("search") //
  .service(
    "MusicSearch",
    /**
     * @param {angular.IHttpService} $http
     */
    function($http, $rootScope) {
      this.url = "https://api.spotify.com/v1/search";

      this.state = {
        query: "",
        results: []
      };

      // this.stateChanges = new Subject();
      this.stateChanges = new BehaviorSubject(this.state);

      this.results$ = this.stateChanges.pipe(
        map(function(state) {
          return state.results;
        }),
      );

      this.search = function(query = "batman") {
        return $http
          .get(this.url, {
            params: {
              type: "album",
              query: query
            }
          })
          .then(function(response) {
            return response.data.albums.items;
          })
          .then(
            function(results) {
              this.state.results = results;
              this.stateChanges.next(this.state);

              // $rootScope.$broadcast("MusicSearch:results", results);

              return results;
            }.bind(this)
          );
      };
    }
  );
