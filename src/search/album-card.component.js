angular
  .module("search") //
  .component("albumCard", {
    bindings: { album: "<" },
    template: `
    <img ng-src="{{$ctrl.album.images[0].url}}"
         class="card-img-top">

    <div class="card-body">
      <h5 class="card-title">{{$ctrl.album.name}}</h5>
    </div>
    `
  });
