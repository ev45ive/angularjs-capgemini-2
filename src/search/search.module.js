var search = angular.module("search", []);

import { Subscription } from "rxjs";

require("./search-form.component");
require("./album-card.component");
require("./search-results.component");
//
require("./music-search.service");

search.controller("SearchCtrl", function(MusicSearch, $scope) {
  this.query = "";
  this.message = "";

  this.subscription = new Subscription();

  this.subscription.add(
    MusicSearch.results$.subscribe(
      function(results) {
        console.log("New Results!");
        this.results = results;
      }.bind(this)
    )
  );

  $scope.$on(
    "$destroy",
    function() {
      this.subscription.unsubscribe();
    }.bind(this)
  );

  // $scope.$on('MusicSearch:results',function(event,results){
  //   this.results = results;
  // })

  // $scope.$watch(
  //   "$ctrl.state.results",
  //   // function() {
  //   //   console.log('çhecking...')
  //   //   return MusicSearch.state.results;
  //   // },
  //   function(results) {
  //     console.log("New Results!");
  //     this.counter = results && results.length;
  //     this.results = results;
  //   }.bind(this)
  // );
  this.search = function(query) {
    $scope.$emit("notification", { message: "Searching for " + query });

    MusicSearch.search(query)

      // p.then(albums => (this.results = albums))

      .then(
        function(albums) {
          this.results = albums;
        }.bind(this)
      )

      .catch(
        function(error) {
          this.message = error.message;
        }.bind(this)
      );
  };
});

// ;
// [
//   {
//     name: "Test 123",
//     images: [{ url: "https://www.placecage.com/gif/200/200" }]
//   },
//   {
//     name: "Test 234",
//     images: [{ url: "https://www.placecage.com/gif/300/300" }]
//   },
//   {
//     name: "Test 345",
//     images: [{ url: "https://www.placecage.com/gif/400/400" }]
//   },
//   {
//     name: "Test 123",
//     images: [{ url: "https://www.placecage.com/gif/200/200" }]
//   },
//   {
//     name: "Test 234",
//     images: [{ url: "https://www.placecage.com/gif/300/300" }]
//   },
//   {
//     name: "Test 345",
//     images: [{ url: "https://www.placecage.com/gif/400/400" }]
//   },
//   {
//     name: "Test 123",
//     images: [{ url: "https://www.placecage.com/gif/200/200" }]
//   },
//   {
//     name: "Test 234",
//     images: [{ url: "https://www.placecage.com/gif/300/300" }]
//   },
//   {
//     name: "Test 345",
//     images: [{ url: "https://www.placecage.com/gif/400/400" }]
//   }
// ];
