// eslint / prettier

angular //
  .module("search") //
  .component("searchForm", {
    template: require("./search-form.tpl.html"),

    bindings: {
      query: "<",
      onSearch: "&"
    },

    controller() {
      this.search = function() {
        if (this.searchForm.$invalid) {
          return;
        }
        
        this.onSearch({ $event: this.query });
      };

      this.handleKey = function(event) {
        if (event.keyCode == 13) {
          this.search();
        }
      };
    }
    // controllerAs:'$ctrl'
  });
