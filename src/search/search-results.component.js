require("./search-results.component.css");

angular
  .module("search") //
  .component("searchResults", {
    bindings: { results: "<" },
    template: `
    <div class="card-group">
      <album-card 
        ng-repeat="result in $ctrl.results"
        album="result"
        class="card">
      </album-card>
    </div>
    `
  });
