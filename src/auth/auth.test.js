require("./auth.module");

// var module = window.module;
var module = angular.mock.module;

describe("Testing - something", () => {
  var Auth, $window, $location;

  beforeEach(module("auth"));

  beforeEach(() => {
    $window = {
      location: { replace: jasmine.createSpy("replace") },
      sessionStorage: jasmine.createSpyObj("sessionStorage", [
        "getItem",
        "setItem",
        "removeItem"
      ])
    };
    $location = jasmine.createSpyObj("$location", ["hash"]);

    module($provide => {
      $provide.value("$window", $window);
      $provide.value("$location", $location);
    });

    inject(function(_Auth_) {
      Auth = _Auth_;
    });
  });

  it("should test the truth", () => {
    expect(true).toBeTruthy();
  });

  it("should create auth service", () => {
    expect(Auth).toBeDefined();
  });

  it("Auth service should get token from sessionsStorage", () => {
    Auth.initialize();
    expect($window.sessionStorage.getItem).toHaveBeenCalledWith("token");
  });

  it("Auth service should redirect when no token", () => {
    Auth._config.auth_url = "testAuthurl";
    Auth.initialize();
    expect($window.location.replace).toHaveBeenCalledWith("testAuthurl?");
  });

  it("Auth retursn token from sesions storate", () => {
    $window.sessionStorage.getItem.and.returnValue(JSON.stringify("placki"));
    Auth.initialize();
    expect(Auth.getToken()).toEqual("placki");
  });
});
