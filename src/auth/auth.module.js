angular
  .module("auth", [])

  // Service - Contructor
  // .service("Auth", AuthenticatorAssertionResponse)

  // Factory - Function creating instance
  // .factory("Auth", function(depA, depB) {
  //  return new AuthenticatorAssertionResponse(depA, depB);
  // });
  .constant("AuthConfig", {
    url: "domyslny"
  });

/* === */

angular
  .module("auth") //
  .service("AuthHttpInterceptor", function(Auth, $q) {
    // https://docs.angularjs.org/api/ng/service/$http#interceptors

    this.request = function(request) {
      request.headers["Authorization"] = "Bearer " + Auth.getToken();
      return request;
    };

    // this.requestError = function(){}

    // this.response = function(){}

    this.responseError = function(err) {
      if (err.status === 401) {
        Auth.authorize();
      }

      return $q.reject(err.data.error);
    };
  })

  .config(function($httpProvider) {
    $httpProvider.interceptors.push("AuthHttpInterceptor");
  })

  .provider("Auth", function() {
    this._config = {};

    this.setConfig = function(config) {
      this._config = config;
    };

    this.$get = function($window, $location) {
      return new Auth(this._config, $window, $location);
    };
  });

function Auth(_config, $window, $location) {
  this._config = _config;
  this.token = "";

  this.initialize = function() {
    var token = $window.sessionStorage.getItem("token");
    if (token) {
      this.token = JSON.parse(token);
    }

    if (!this.token && $location.hash()) {
      var p = new URLSearchParams($location.hash());
      this.token = p.get("access_token");
      $location.hash("");
    }

    if (!this.token) {
      this.authorize();
    } else {
      $window.sessionStorage.setItem("token", JSON.stringify(this.token));
    }
  };

  this.authorize = function() {
    $window.sessionStorage.removeItem("token");

    var p = new URLSearchParams(this._config.params);
    var url = this._config.auth_url + "?" + p.toString();

    $window.location.replace(url);
  };

  this.getToken = function() {
    return this.token;
  };
}
