import angular from "angular";
// https://docs.angularjs.org/api/ngMessages
import "angular-messages";
// Router
import "@uirouter/angularjs";

import "./playlists/playlists.module";
import "./search/search.module";
import "./auth/auth.module";
import "./users/users.module";

export var app = angular.module(
  "app", //
  ["ngMessages", "ui.router", "playlists", "search", "auth", "users"]
);

app.controller("AppCtrl", function($scope, $timeout) {
  $scope.user = {
    name: "Alice"
  };
  $scope.notifications = [];
  $scope.$on("notification", function(event, notification) {
    $scope.notifications.push(notification);
    $timeout(function() {
      $scope.notifications.shift();
    }, 2000);
  });

  $scope.views = [
    {
      label: "Playlists",
      url: "src/playlists/playlists-view.tpl.html"
    },
    {
      label: "Search",
      url: "src/search/search-view.tpl.html"
    }
  ];

  $scope.activeView = $scope.views[0];

  $scope.changeView = function(view) {
    $scope.activeView = view;
  };
});

app.config(function(AuthProvider) {
  AuthProvider.setConfig({
    auth_url: "https://accounts.spotify.com/authorize",
    params: {
      client_id: "70599ee5812a4a16abd861625a38f5a6",
      response_type: "token",
      redirect_uri: "http://localhost:8080/",
      // scopes:['permissionUtworzPlayliste'],
      // state: '{goBackToPLaylists:1}',
      show_dialog: true
    }
  });
  // https://developer.spotify.com/dashboard/
  // placki@placki.com
  // ******
});

app.run(function(Auth) {
  Auth.initialize();
});

app.component("aboutView", {
  bindings: { asyncData: "<", zamiastStringa: "<", onPlacki: "&" },
  template: `<h1>About</h1> <p ng-click="$ctrl.onPlacki()">{{$ctrl.asyncData}}</p>`
});

app.config(function($stateProvider, $urlServiceProvider) {
  // Redirects
  // https://ui-router.github.io/ng1/docs/latest/classes/url.urlrules.html#when
  $urlServiceProvider.rules.when("", "/");
  $urlServiceProvider.rules.when("placki", "/");
  $urlServiceProvider.rules.otherwise("/");

  // States
  $stateProvider
    .state({
      name: "root",
      url: "/",
      template: `<h1>Home</h1>`,
      controller: function($scope) {}
    })
    .state({
      name: "about",
      url: "/about",
      resolve: {
        asyncData: function($http, $q, $state, $stateParams) {
          return $q(function(resolve, reject) {
            setTimeout(function() {
              reject("Lubie placki!");
            }, 2000);
          });
        },
        syncData: function() {
          return { name: "placi" };
        },
        onPlacki: function() {
          return function() {
            console.log("placki");
          };
        }
      },
      component: "aboutView"
    })
    .state({
      name: "search",
      url: "/search",
      templateUrl: "src/search/search-view.tpl.html",
      controller: "SearchCtrl as $ctrl"
    })
    .state({
      name: "playlists",
      url: "/playlists",
      templateUrl: "src/playlists/playlists-view.tpl.html"
    })

});


// .state({
//   name: "users",
//   url: "/users",
//   redirectTo: "users.index",
//   template: `<div class="row">
//       <div class="col"><h1>Users</h1></div>
//     </div>
//     <div class="row">
//       <div class="col">
//         <ui-view></ui-view>
//       </div>
//   </div>`
// })
// .state({
//   // parent: "users",
//   // name: "index",
//   name: "users.index", // ui-sref="users.index"
//   template: `<h2>child view</h2>`
// })
// .state({
//   name: "users.details", // ui-sref="users.index"
//   url: "/details",
//   template: `<h2>child details</h2>`
// });